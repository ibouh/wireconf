# Wireguard experimentation tool

This tool will allow you to orchestrate experiments for wireguard (setup config, ). It was built progressively with focus on results, so might not fit all good engineering practices and you may have 1 or 2 hardcoded scenarios (which you can expand if you wish).

## Experiments

During an experiment, for every wireguard version specified and for every payload and throughput value specified the configured version of wireguard is installed and configured then `n_clients` clients are spawned across `n_machines`. These are used to inject traffic to the target web server via the wireguard server. During this traffic injection process that lasts `duration` seconds, CPU and network information are collected, parsed and stored to files which are copied to the orchestrating machine (currently only server stats are considered). This data is later used to produce corresponding plots and graphics. We have 3 experiment scenarios:

- Multiple throughput scenario: payload size is fixed but number of requests/s per client is varied based on `throughputs` setting.

- Multiple payload scenario: number of requests/s per client is fixed but payload size is varied based on `payloads` setting.

- Streaming scenario: Special case of multiple payload scenario specified using the `stream` setting. Here a single streaming connection is made but the `payloads` setting defines the size of each "chunk" sent per second.

## Installation

I recommend using a virtual environment with python3.12.

`conda create -n wirexp python=3.12 -y && conda activate wirexp`

Dependencies are available from requirements.txt file

`pip3 install -r requirements.txt`

## Usage

A configuration file allows you to define what your experiment is all about, the wirexp.example.ini provides with a sample and its comments describe the use of each setting. There after running an experiment is as simple as running:

`python3 wirexp xp:run ./xp_conf/100_clients_10s_machine_streaming.ini`

Concerning authentication, it's the user's care to make sure the machine running the script has ssh configured properly to have access to the experiment's machines.

### Commands

| Command | Description |
|---------|---------|
|`python3 wirexp xp:run config_file` | Run an experiment based on config_file settings. |
|`python3 wirexp xp:plot config_file` | Plot experiment graphics based on config_file settings. |
|`python3 wirexp xp:prepare site` | Setup wirexp tool on `site` G5K frontend. This will clone tool repo setup a wirexp conda environment and install required dependencies. The command expects to be able to connect to host `site_name` (and without password) so ssh configured accordingly or you can run it from G5K access site or another frontend. |
|`python3 wirexp xp:deploy config_file` | Use this command to deploy experiment on G5K. It will take care of reserving machines and setting up experiment. |
|`python3 wirexp xp:swarm "config_file_pattern"` | Run multiple experiment deployments in background for every config file matched by specified glob pattern. |

### Locally

If you're working locally, your goto command are `xp:run` and `xp:plot`. Authentification is possible via username and password but you can also specify the path to a custom private key to use via `private_key` setting in case the default one won't do. The user is responsible for specifying the ip of the machines to use via the `machine_{x}_ip` setting for client machines, `server_ip` and `target_ip` for server and target machines respectively. Moreover, the same credentials are used for connecting to all the machines of the experiment.

### Grid5000

Working on G5K is a lot easier and so far the tool is optimized for G5K usage. You only need to specify the site and cluster to run on using the corresponding settings. However you will need to launch the script from the corresponding site frontend or node, as this is repetitive you may use the `python3 wirexp xp:prepare site_name` to setup the tool on this frontend.

### Debugging

You can add instructions in the `xp_run` function of wirexp file to run any given command on any machine or print out the output of a variable or setting. If need be, you can use the `pause` function that will pause and print out the connection details so you can connect to the machines individually and check what's going on.

## Settings

|Setting | Default | Description |
|---------|:---------:|-------------|
|section | [auth] ||
| `username` |  -  | Username to use when connecting to the xp machines |
| `password` |  -  | Password to use when connecting to the xp machines (required only in case of password based authentication) |
| `private_key` |  -  | path to custom private key to use to connect (key pair authentication) |
|section | [xp] ||
| `dest` |  `./config_file_basename`  | Path to the folder where all experiment files and data will be copied. |
| `linux_version` |  `6.8`  | The linux kernel version to work with |
| `versions` |  -  | Comma separated list of wireguard versions to work on, hte experiment will be run for each of these versions. A corresponding branch should exist on the  [wireguard repository](https://gitlab.inria.fr/ibouh/wireguard) in the form `version`-`linux_version` for example if you specified `linux_version=6.8` and `versions=encryptes,unencrypted` the script try to pull the `encrypted-6.8` and `unencrypted-6.8` branches respectively|
| `trace` |  `False`  | Wether to trace functions using trace-cmd or not. This allows you to have more fine grain split of time across wireguard functions. This setting is deprecated and may not work properly.   |
| `track_functions` |  -  | Comma separated list of functions whose execution times should be tracked. This is used only when tracing is active (`trace=True`) |
| `n_machines` |  `n_clients`  | Number of client machines to use |
| `n_clients` |  `1`  | Number of wireguard clients (and injectors)  to spawn. You may have multiple clients per machine and they will be spread evenly across the number of machines specified |
| `server_ip` |  -  | Ip of the machine that will serve as  wireguard server |
| `target_ip` |  -  | Ip of the machine that will serve as target web server |
| `machine_{x}_ip` |  -  | Ip of the client machines, where `{x}` is the corresponding index from `1...n` (`n=n_machines`) |
| `network_interface` |  `eno1`  | The name of the physical/ethernet network interface to use. wireguard will be configured to forward all request to this interface. It should be identical across all xp machines. Generally fallback to `eno1` on deployed nodes (G5K) but can differ from one cluster/site to the other.|
| `throughput` |  `50`  | The number of request/s sent by each client. This doesn't work in the stream scenario. |
| `throughputs` |  -  | A comma separated list of throughputs to use. When specified this will trigger a multiple throughput scenario where the experiment is ran for each throughput specified. This doesn't work in the stream scenario and cannot be used simultaneously with `payloads` setting. |
| `payload` |  `1`  | The payload size (in MB) sent with each request. Decimal values are allowed. This is used to define `chunk_size` in streaming scenario. |
| `payloads` |  -  | A comma separated list of throughputs to use. When specified this will trigger a multiple payload scenario where the experiment is ran for each payload specified. This doesn't work in the stream scenario and cannot be used simultaneously with `throughputs` setting. |
| `stream` |  -  | When set defines a stream scenario, can take value `stream` or `range` for chunk transfer encoding or range request streaming respectively. So far only `stream` is supported `range` is implemented but not fully integrated. |
| `duration` |  -  | The duration (in seconds) of each experiment (duration of traffic injection). |
| `calibration_time` |  `5`  | The time (in seconds) between when traffic injection is started and when measurement starts. This isn't considered (and thus not included in) as part of the experiment duration. |
| `delta` |  `5`  | Used to define the time to wait between some coupled operations like checking if data collectors are sone saving collected data to files. |
| `active_cores` |  `0`  | The number of cores to use on all experiments machine. A number greater than the actual number of cores on the machine  will cause an error. Setting this field to 0 will instruct the script to use all cores but I recommend setting the actual value because it is required for plotting |
| `client_cores` |  `active_cores`  | The number of cores to use on client machines. This overrides the `active_cores` but only for client machines. |
| `server_cores` |  `active_cores`  | The number of cores to use on wireguard server machine. This overrides the `active_cores` but only for the server machine. |
| `target_cores` |  `active_cores`  | The number of cores to use on target machine. This overrides the `active_cores` but only for target machine. |
| `threaded` |  `False`  | Wether to manually activate NAPI poller based threading by writing to `/sys/class/net/wg_interface_name/threaded` files. This setting is deprecated in favour of built-in activation in the corresponding wireguard versions (encrypted-threaded, unencrypted-threaded) and will not work in the current version correctly. |
| `hyperthreading` |  `False`  | Wether to activate hyperthreading or not during experiment. |
| `tls` |  `True`  | Wether to send traffic via http or https. Currently streaming only supports https. |
| `site` |  -  | When running on grid5000, use this to specify the site to run the experiment on. |
| `cluster` |  -  | When running on grid5000, use this to specify the cluster to run the experiment on. |
|section | [plots] | These settings are particularly for base scenarios (not multiple payload/throughput scenarios)|
| `cpu` |  -  | Comma separated list of cpu plots to perform in the form `agg_resampleperiod_rolls`, like `mean_1_40-50,mean_5_5-10` will produce 4 plots: The first two will show the average cpu usage per second with a after a rolling mean with a window of 40s and 50s respectively has been applied to it. The last two will show the average cpu usage per 5s after a rolling mean with a window of 5s and 10s respectively has been applied to it. Other aggregates available are: min, max and median. |
| `throughput` |  -  | Comma separated list of throughput plots to perform in the form `agg_resampleperiod_rolls`, like `mean_1_40-50,mean_5_5-10` will produce 4 plots: The first two will show the average cpu usage per second with a after a rolling mean with a window of 40s and 50s respectively has been applied to it. The last two will show the average cpu usage per 5s after a rolling mean with a window of 5s and 10s respectively has been applied to it. Other aggregates available are: min, max and median. |
| `cpu_times` |  True  | Wether to produce cpu times plots. This is only possible if tracing is/was active during the experiment. |

## Recommended sites/clusters to use on G5K

| Site | Cluster(# nodes) | NICs | # cores|
|---------|:--------------:|:-------:|:---:|
| Rennes |  paravance (72)  | 2 x 10Gb/s (Intel 82599ES 10-Gigabit SFI/SFP+ Network Connection, driver: ixgbe, SR-IOV enabled)| 2 x 8 cores (Intel Xeon E5-2630 v3 (Haswell), x86_64, 2.40GHz) |
| Rennes |  parasilo (27)  | 2 x 10Gb/s (Intel 82599ES 10-Gigabit SFI/SFP+ Network Connection, driver: ixgbe, SR-IOV enabled) | 2 x 8 (Intel Xeon E5-2630 v3 (Haswell), x86_64, 2.40GHz) |
| Nantes |  ecotype (48)  | 10Gb/s (Intel 82599ES 10-Gigabit SFI/SFP+ Network Connection, driver: ixgbe, SR-IOV enabled) | 2 x 8 (Intel Xeon E5-2630L v4 (Broadwell), x86_64, 1.80GHz) |
| Nantes |  econome (22)  | 10Gb/s (Intel 82599ES 10-Gigabit SFI/SFP+ Network Connection, driver: ixgbe, SR-IOV enabled) | 2 x 8 (Intel Xeon E5-2660 (Sandy Bridge), x86_64, 2.20GHz) |
| Nancy  |  gros (124)  | 2 x 25Gb/s (Mellanox Technologies MT27710 Family [ConnectX-4 Lx], driver: mlx5_core, SR-IOV enabled) | 18 (Intel Xeon Gold 5220 (Cascade Lake-SP), x86_64, 2.20GHz) |
