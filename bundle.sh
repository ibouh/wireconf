#!/bin/bash

dir=$1

# create ${dir}_bundle directory if not exists
mkdir -p ${dir}_bundle

# copy plots
for f in $(ls $dir); do
	# check if $f is a directory and contains plots folder
    if [ -d "$dir/$f" ] && [ -d "$dir/$f/plots" ]; then
        mkdir -p ${dir}_bundle/$f
        cp -r $dir/$f/plots/* ${dir}_bundle/$f/
    fi
done

# tar the bundle
tar -czf ${dir}_bundle.tar.gz -C ${dir}_bundle .

# remove the bundle folder
rm -rf ${dir}_bundle