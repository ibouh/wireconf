import subprocess
import os
from mando import command
from python_wireguard import Key

# Set your vpn tunnel network (example is for 10.99.99.0/24)
ipnet_tunnel_1 = 10
ipnet_tunnel_2 = 99
ipnet_tunnel_3 = 99
ipnet_tunnel_4 = 0
ipnet_tunnel_cidr = 24


@command("make:config")
def generate_conf(
    endpoint=None,
    interface=None,
    target=None,
    clients=1,
    path="",
    port=52820,
    allowed_ips="0.0.0.0/0, ::/0",
    dns="",
    tunnel_0_0_0_0=False,
):

    wg_priv_keys = []
    wg_pub_keys = []

    # Gen-Keys
    for _ in range(clients + 1):
        (privkey, pubkey) = Key.key_pair()
        wg_priv_keys.append(privkey)
        wg_pub_keys.append(pubkey)

    server_private_ip = (
        f"{ipnet_tunnel_1}.{ipnet_tunnel_2}.{ipnet_tunnel_3}.{ipnet_tunnel_4+1}"
    )

    ################# Server-Config ##################
    server_config = (
        "[Interface]\n"
        f"Address = {server_private_ip}/{ipnet_tunnel_cidr}\n"
        f"ListenPort = {port}\n"
        f"PrivateKey = {wg_priv_keys[0]}\n"
        f"Table = {port}\n"
    )
    if interface:
        server_config += (
            f"PostUp = iptables -A FORWARD -i %i -j ACCEPT; iptables -t nat -A POSTROUTING -o {interface} -j MASQUERADE\n"
            f"PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -t nat -D POSTROUTING -o {interface} -j MASQUERADE\n"
        )

    for i in range(1, clients + 1):
        server_config += (
            f"[Peer]\n"
            f"PublicKey = {wg_pub_keys[i]}\n"
            f"AllowedIPs = {ipnet_tunnel_1}.{ipnet_tunnel_2}.{ipnet_tunnel_3}.{ipnet_tunnel_4+1+i}/32\n"
        )

    # print("*"*10 + " Server-Conf " + "*"*10)
    # print(server_config)
    os.makedirs(os.path.join(path, "server"), exist_ok=True)
    with open(os.path.join(path, "server/wg.conf"), "wt") as f:
        f.write(server_config)

    ################# Client-Config ##################
    client_configs = []
    client_ips = []
    for i in range(1, clients + 1):
        client_ip = (
            f"{ipnet_tunnel_1}.{ipnet_tunnel_2}.{ipnet_tunnel_3}.{ipnet_tunnel_4+1+i}"
        )
        client_ips.append(client_ip)
        client_port = port + i
        client_config = (
            f"[Interface]\n"
            f"Address = {client_ip}/24\n"
            f"ListenPort = {client_port}\n"
            f"PrivateKey = {wg_priv_keys[i]}\n"
            f"Table = {client_port}\n"
        )

        if target:
            client_config += (
                f"PostUp =  iptables -A FORWARD -i %i -j ACCEPT; iptables -t nat -A POSTROUTING -o eno1 -j MASQUERADE; ip rule add from {client_ip} lookup {client_port} \n"
                f"PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -t nat -D POSTROUTING -o eno1  -j MASQUERADE; ip rule add from {client_ip} lookup {client_port} \n"
            )

        if dns:
            client_config += f"DNS = {dns}\n"

        client_config += f"[Peer]\n" f"PublicKey = {wg_pub_keys[0]}\n"

        if tunnel_0_0_0_0 is False:
            client_config += f"AllowedIPs = {allowed_ips}, {server_private_ip}/32\n"
        else:
            client_config += "DNS = 0.0.0.0/0\n"

        client_config += f"Endpoint = {endpoint}:{port}\n" if endpoint else ""
        client_configs.append(client_config)

        # print("*"*10 + f" Client-Conf {i} " + "*"*10)
        # print(client_config)
        file_dir = os.path.join(path, f"client_{i}")
        os.makedirs(file_dir, exist_ok=True)
        with open(os.path.join(file_dir, "wg.conf"), "wt") as f:
            f.write(client_config)
    return {
        "server_ip": server_private_ip,
        "client_ips": client_ips,
    }
