#enable data collection with sysstat:

#check app running on given port
sudo netstat -peanut | grep ":443 "

#ingestion des données:
sadf -dhT ./test.dat 1 -- -n DEV --iface=eno1
sadf -dhT ./without_encryption.sar 1 -- -u -r -n DEV > without_encryption.csv
trace-cmd record -p function_graph -l :mod:wireguard -- sleep 300
trace-cmd report -R


p#ackages git

# install new kernel version
sudo apt-get install linux-image-6.5.0-26-generic linux-headers-6.5.0-26-generic linux-modules-extra-6.5.0-26-generic

# Get kernel source code
apt-get source linux-source-6.5.0

# install kernel headers
sudo apt-get install linux-headers-$(uname -r)

ssh root@node.site.grid5000.fr

kadeploy3 ubuntu2204-min

ip a | awk '$2 ~ /enp[A-z0-9]+:/ { print $2; exit; }' # get physical network interface name

oarsub -I -l host=1,walltime=1:45 -t deploy

# Setup gcc-12
sudo apt install --reinstall gcc-12 -y
sudo ln -s -f /usr/bin/gcc-12 /usr/bin/gcc

# compiling kernel
sudo apt-get install git fakeroot build-essential ncurses-dev xz-utils libssl-dev bc flex libelf-dev bison
cp -v /boot/config-$(uname -r) .config
make menuconfig
scripts/config --disable SYSTEM_TRUSTED_KEYS
scripts/config --disable SYSTEM_REVOCATION_KEYS
make

# wireconf deployment config generation command
python3 wireconf make:deployment  131.254.212.32 131.254.212.35 eno1

# send traffic with wrk2
wrk -t 4 -c 4 -d 360s -R 100 -L https://131.254.212.35/index > without_encryption_load_output.wrk2

# disable hyperthreading
echo off > /sys/devices/system/cpu/smt/control

# enable hyperthreading
echo on > /sys/devices/system/cpu/smt/control

# disable all cores except 0 and 1
for i in $(seq 2 3); do echo 0 > /sys/devices/system/cpu/cpu$i/online; done

# enable all cores
for i in $(seq 0 3); do echo on > /sys/devices/system/cpu/cpu$i/online; done

# parse sar data to csv
sadf -dhT ./without_encryption_client.sar 1 -- -u -r -n DEV > without_encryption_client.csv
sadf -dhT ./without_encryption-all_cpus_on.sar 1 -- -u -r -n DEV > without_encryption-all_cpus_on.csv

# start load testing for 6 minutes
wrk -t 4 -c 4 -d 360s -R 100 https://131.254.212.35/index

# record the function graph of wireguard
trace-cmd record -p function_graph -l :mod:wireguard -- sleep 10
trace-cmd record -p function_graph -g encrypt_packet -- sleep 10

# report the function graph of wireguard with function names and filter encryption function
trace-cmd report -O fgraph:tailprint=yes | grep encrypt_packet
trace-cmd report -O fgraph:tailprint=yes | grep chacha20poly1305_encrypt_sg_inplace


sar -o ./without_encryption_server.sar -u -r -n DEV 1 300 &
sar -o ./without_encryption_client.sar -u -r -n DEV 1 300 &

# remove module wireguard
rmmod wireguard.ko

# install module wireguard
insmod wireguard.ko
insmod wireguard-linux/drivers/net/wireguard/wireguard.ko

# compile wireguard external module (in wireguard directory)
make -C /lib/modules/`uname -r`/build M=$PWD
# install wireguard external module (in wireguard directory)
make -C /lib/modules/`uname -r`/build M=$PWD modules_install




# client 
echo "off" > "/sys/devices/system/cpu/smt/control"
for i in 2 3; do echo "off" > "/sys/devices/system/cpu/cpu$i/online"; done
## with encryption
wrk -t $(nproc) -c $(nproc) -R 50 -d 360 -L https://131.254.212.35/index > with_encryption_load_output.wrk2 &
rm ./with_encryption_client.sar
sar -u -r -n DEV -o ./with_encryption_client.sar 1 300 && sleep 300
sadf -dhT ./with_encryption_client.sar 1 -- -u -r -n DEV > with_encryption_client.csv
## without encryption
wrk -t $(nproc) -c $(nproc) -R 50 -d 360 -L https://131.254.212.35/index > without_encryption_load_output.wrk2 &
rm ./without_encryption_client.sar
sar -u -r -n DEV -o ./without_encryption_client.sar 1 300 && sleep 300
sadf -dhT ./without_encryption_client.sar 1 -- -u -r -n DEV > without_encryption_client.csv


# server
echo "off" > "/sys/devices/system/cpu/smt/control"
for i in 2 3; do echo "off" > "/sys/devices/system/cpu/cpu$i/online"; done
## with encryption
rm ./with_encryption_server.sar
sar -u -r -n DEV -o ./with_encryption_server.sar 1 300 && sleep 300
sadf -dhT ./with_encryption_server.sar 1 -- -u -r -n DEV > with_encryption_server.csv
## without encryption
rm ./without_encryption_server.sar
sar -u -r -n DEV -o ./without_encryption_server.sar 1 300 && sleep 300
sadf -dhT ./without_encryption_server.sar 1 -- -u -r -n DEV > without_encryption_server.csv


# stupid xp
sar -u -n DEV,EDEV -o ./xp.sar 1 300

# calculate total cpu time in nanoseconds
trace-cmd report -R | grep $(trace-cmd report -f | grep -w "encrypt_packet" | awk '{ print $1 }') | grep funcgraph_exit | sed -e 's/calltime=//g' -e 's/rettime=//g' | gawk -F: '{ num += strtonum($10) - strtonum($9) } END { print num }' > encrypt_time

# mod install using modprobe
sudo cp wireguard-linux/drivers/net/wireguard/wireguard.ko /lib/modules/`uname -r`/
sudo depmod
sudo modprobe wireguard

# replot all xps
for config in ./xps/*.ini; do python3 wirexp xp:plot "$config"; done

# bundle results
./bundle.sh "/home/bouhivan/projects/wireguard/results-G5K/new_results_after_fix"

# setup target
git clone https://gitlab.inria.fr/ibouh/wirexp-target.git
cd wirexp-target
rm -rf payload
dd if=/dev/zero of=payload bs=1M seek=10 count=0
curl -sL https://deb.nodesource.com/setup_18.x -o node_source_setup.sh && sudo-g5k bash node_source_setup.sh && sudo-g5k apt install nodejs -y
npm install && sudo-g5k screen -d -m node index.js > target.log 2>&1 &

# setup client
wget https://github.com/tsenart/vegeta/releases/download/v12.11.1/vegeta_12.11.1_linux_amd64.tar.gz && tar xvzf vegeta_12.11.1_linux_amd64.tar.gz
sudo-g5k cp vegeta /usr/bin
dd if=/dev/zero of=payload bs=1M seek=10 count=0
echo "POST https://172.16.192.5:443/"  > targets.txt
nohup vegeta -cpus $(nproc) attack -insecure -rate=5 -duration=50000s -body payload -keepalive -targets targets.txt > /dev/null 2>&1 &
sar -n DEV 1

# misc
sudo-g5k pkill -f vegeta
ps -aux | grep vegeta

# reserving
https://intranet.grid5000.fr/oar/Nancy/monika.cgi
oarsub -p "host IN (gros-16)" -I
oarsub -I -l host=1,walltime=1:45 -t deploy
oarsub -I -p paravance -l host=1,walltime=1:45 -t deploy
kadeploy3 ubuntu2404-min
SRC_ADDRESS="131.254.22.96" TARGET="https://131.254.22.96" node stream.js
SRC_ADDRESS="172.16.97.8" TARGET="https://172.16.97.7" node stream.js
curl -k --interface wgc https://172.16.97.7